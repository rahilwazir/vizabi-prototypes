# vizabi-prototypes

This project will contains visualizations built on miso d3.charts similar to those of vizabi project

This project is generated with [yo angular generator](https://github.com/yeoman/generator-angular)
version 0.11.1.

## Initial Set up

Run npm install, followed by bower install to obtain all the necessary dependencies and css components.

## Build & development

Run `grunt` for building and `grunt serve` for preview.

## Testing

Running `grunt test` will run the unit tests with karma.
