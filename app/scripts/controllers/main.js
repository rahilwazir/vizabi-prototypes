'use strict';

/**
 * @ngdoc function
 * @name vizabiPrototypesApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the vizabiPrototypesApp
 */
angular.module('vizabiPrototypesApp')
  .controller('MainCtrl', function ($scope, $location) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];

    $scope.isActive = function (viewLocation) {
      return viewLocation === $location.path();
    };
  });
