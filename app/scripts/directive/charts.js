(function() {
  'use strict';

  //Default states
  var barObj = {
    "state": {
      "marker": {
        "dimensions": [
          "entities",
          "time"
        ],
        "label": {
          "use": "property",
          "value": "geo.name"
        },
        "axis_y": {
          "use": "indicator",
          "value": "lex",
          "unit": "",
          "scaleType": "linear"
        },
        "axis_x": {
          "use": "property",
          "value": "geo.name",
          "unit": ""
        },
        "color": {
          "use": "property",
          "value": "geo.region",
          "unit": "",
          "palette": {
            "_default": "#bb2600",
            "world": "#ffb600",
            "eur": "#FFE700",
            "afr": "#00D5E9",
            "asi": "#FF5872",
            "ame": "#7FEB00"
          }
        }
      },
      "time": {
        "start": 1953,
        "end": 2012,
        "value": 1990,
        "playable": true,
        "playing": false,
        "loop": false,
        "round": true,
        "speed": 300,
        "unit": "year",
        "step": 1
      }
    },
    "data": {
      "reader": 'local-json',
      "path": 'geo-time-lex'
    },
    "language": {
      "id": "en",
      "strings": {
        "en": {
          "title": "Bar Chart",
          "buttons/expand": "Full screen"
        },
        "pt": {
          "title": "Bolha Gráfico Título",
          "buttons/expand": "Tela cheia"
        }
      }
    }
  };
  var bubbleObj = {
    "state": {
      "marker": {
        "dimensions": [
          "entities",
          "time"
        ],
        "type": "geometry",
        "shape": "circle",
        "label": {
          "use": "property",
          "which": "geo.name"
        },
        "axis_y": {
          "use": "indicator",
          "unit": "years",
          "which": "lex",
          "scaleType": "linear"
        },
        "axis_x": {
          "use": "indicator",
          "unit": "$/year/person",
          "which": "gdp_per_cap",
          "scaleType": "log"
        },
        "color": {
          "use": "property",
          "unit": "",
          "palette": {
            "asi": "#FF5872",
            "eur": "#FFE700",
            "ame": "#7FEB00",
            "afr": "#00D5E9",
            "_default": "#ffb600"
          },
          "which": "geo.region",
          "scaleType": "ordinal"
        },
        "size": {
          "use": "indicator",
          "unit": "",
          "which": "pop",
          "scaleType": "linear",
          "max": 0.75
        }
      },
      "time": {
        "value": "2000",
        "start": "1990",
        "end": "2014",
        "playable": true,
        "playing": false,
        "loop": false,
        "round": "ceil",
        "speed": 300,
        "unit": "year",
        "formatInput": "%Y",
        "step": 1,
        "adaptMinMaxZoom": false,
        "trails": false,
        "lockNonSelected": 0
      },
      "entities": {
        "show": {
          "dim": "geo",
          "filter": {
            "geo": [
              "afg",
              "alb",
              "dza",
              "ago",
              "atg",
              "arg",
              "arm",
              "abw",
              "aus",
              "aut",
              "aze",
              "bhs",
              "bhr",
              "bgd",
              "brb",
              "blr",
              "bel",
              "blz",
              "ben",
              "btn",
              "bol",
              "bih",
              "bwa",
              "bra",
              "chn",
              "brn",
              "bgr",
              "bfa",
              "bdi",
              "khm",
              "cmr",
              "can",
              "cpv",
              "caf",
              "tcd",
              "_cis",
              "chl",
              "col",
              "com",
              "cod",
              "cog",
              "cri",
              "civ",
              "hrv",
              "cub",
              "cyp",
              "cze",
              "dnk",
              "dji",
              "dom",
              "ecu",
              "egy",
              "slv",
              "gnq",
              "eri",
              "est",
              "eth",
              "fji",
              "fin",
              "fra",
              "guf",
              "pyf",
              "gab",
              "gmb",
              "geo",
              "deu",
              "gha",
              "grc",
              "grd",
              "glp",
              "gum",
              "gtm",
              "gin",
              "gnb",
              "guy",
              "hti",
              "hnd",
              "hkg",
              "hun",
              "isl",
              "ind",
              "idn",
              "irn",
              "irq",
              "irl",
              "isr",
              "ita",
              "jam",
              "jpn",
              "jor",
              "kaz",
              "ken",
              "kir",
              "prk",
              "kor",
              "kwt",
              "kgz",
              "lao",
              "lva",
              "lbn",
              "lso",
              "lbr",
              "lby",
              "ltu",
              "lux",
              "mac",
              "mkd",
              "mdg",
              "mwi",
              "mys",
              "mdv",
              "mli",
              "mlt",
              "mtq",
              "mrt",
              "mus",
              "myt",
              "mex",
              "fsm",
              "mda",
              "mng",
              "mne",
              "mar",
              "moz",
              "mmr",
              "nam",
              "npl",
              "nld",
              "ant",
              "ncl",
              "nzl",
              "nic",
              "ner",
              "nga",
              "nor",
              "omn",
              "pak",
              "pan",
              "png",
              "pry",
              "per",
              "phl",
              "pol",
              "prt",
              "pri",
              "qat",
              "reu",
              "rou",
              "rus",
              "rwa",
              "lca",
              "vct",
              "wsm",
              "stp",
              "sau",
              "sen",
              "srb",
              "syc",
              "sle",
              "sgp",
              "svk",
              "svn",
              "slb",
              "som",
              "zaf",
              "sds",
              "esp",
              "lka",
              "sdn",
              "sur",
              "swz",
              "swe",
              "che",
              "syr",
              "twn",
              "tjk",
              "tza",
              "tha",
              "tls",
              "tgo",
              "ton",
              "tto",
              "tun",
              "tur",
              "tkm",
              "uga",
              "ukr",
              "are",
              "gbr",
              "usa",
              "ury",
              "uzb",
              "vut",
              "ven",
              "pse",
              "esh",
              "vnm",
              "vir",
              "yem",
              "zmb",
              "zwe"
            ],
            "geo.cat": [
              "country"
            ]
          }
        },
        "select": [],
        "brush": [],
        "opacitySelectDim": 0.3,
        "opacityRegular": 0.8
      }
    },
    "data": {
      "reader": "local-json",
      "path": "basic-indicators"
    },
    "language": {
      "id": "en",
      "strings": {
        "en": {
          "title": "Bubble Chart Title",
          "buttons/expand": "Full screen"
        },
        "pt": {
          "title": "Bolha Gráfico Título",
          "buttons/expand": "Tela cheia"
        }
      }
    }
  };
  var lineObj = {
    state: {
      "marker": {
        "dimensions": [
          "entities",
          "time"
        ],
        "label": {
          "use": "property",
          "value": "geo.name"
        },
        "axis_y": {
          "use": "indicator",
          "unit": "",
          "which": "gdp_per_cap",
          "scaleType": "log"
        },
        "axis_x": {
          "use": "indicator",
          "unit": "",
          "which": "time",
          "scaleType": "time"
        },
        "color": {
          "use": "property",
          "value": "geo.region",
          "unit": "",
          "palette": {
            "_default": "#bb2600",
            "world": "#ffb600",
            "eur": "#FFE700",
            "afr": "#00D5E9",
            "asi": "#FF5872",
            "ame": "#7FEB00"
          },
          "which": "geo.region"
        },
        "color_shadow": {
          "use": "property",
          "unit": "",
          "palette": {
            "_default": "#fbbd00",
            "world": "#fb6d19",
            "eur": "#fbaf09",
            "afr": "#0098df",
            "asi": "#da0025",
            "ame": "#00b900"
          },
          "which": "geo.region"
        }
      },
      "entities": {
        "show": {
          "geo": [
            "*"
          ],
          "geo.cat": [
            "region"
          ]
        },
        "select": [],
        "brush": [],
        "opacitySelectDim": 0.3,
        "opacityRegular": 0.8,
        "dim": "geo"
      },
      "time": {
        "value": "2012",
        "start": "1990",
        "end": "2014",
        "playable": true,
        "playing": false,
        "loop": false,
        "round": true,
        "speed": 300,
        "unit": "year",
        "formatInput": "%Y",
        "step": 1,
        "adaptMinMaxZoom": false
      }
    },
    data: {
      reader: 'local-json',
      path: 'basic-indicators'
    },
    language: {
      id: "en",
      strings: {
        en: {
          "title": "Line Chart",
          "buttons/expand": "Full screen"
        },
        pt: {
          "title": "Gráfico de linha",
          "buttons/expand": "Tela cheia"
        }
      }
    }
  };

  function barChart() {
    var VPBarChart = new VizabiPrototype("bar-chart", "#bargraph", barObj);
    document.getElementById('stateChange').onclick = function(){
      VPBarChart.stateChanger(barObj);
    };
  }

  function lineChart() {
    var VPLineChart = new VizabiPrototype("line-chart", "#linechart", lineObj);
    document.getElementById('stateChange').onclick = function(){
      VPLineChart.stateChanger(lineObj);
    };
  }

  function bubbleChart() {
    var VPBubbleChart = new VizabiPrototype("bubble-chart", "#bubblechart", bubbleObj);
    document.getElementById('stateChange').onclick = function(){
      VPBubbleChart.stateChanger(bubbleObj);
    };
  }

  angular.module('vizabiPrototypesApp')
    .directive('barchart', function () {
      return {
        link: function() {
          barChart();
        }
      };
    })
    .directive('linechart', function () {
      return {
        link: function () {
          lineChart();
        }
      };
    })
    .directive('bubblechart', function () {
      return {
        link: function () {
          bubbleChart();
        }
      };
    });
})();
