'use strict';

/**
 * @ngdoc overview
 * @name vizabiPrototypesApp
 * @description
 * # vizabiPrototypesApp
 *
 * Main module of the application.
 */
angular
  .module('vizabiPrototypesApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch'
  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/about', {
        templateUrl: 'views/about.html',
        controller: 'AboutCtrl'
      })
      .when('/barchart', {
        templateUrl: 'views/barchart.html',
        controller: 'BarChartCtrl'
      })
      .when('/linechart', {
        templateUrl: 'views/linechart.html',
        controller: 'LineChartCtrl'
      })
      .when('/bubblechart', {
        templateUrl: 'views/bubblechart.html',
        controller: 'BubbleChartCtrl'
      })
      .otherwise({
        redirectTo: '/barchart'
      });
  });
