(function () {
  'use strict';

  /**
   * Initialize Bar Chart
   * @param id {String}
   * @param options {Object}
   */
  VizabiPrototype.prototype.LineChart = function () {
    var options = this.options;
    var htmlID = this.htmlID;
    var dataset = this.dataset;

    document.getElementById("stateText").value = JSON.stringify(options, null, ' ');

    //State Options
    var opacityRegular = options.state.entities.opacityRegular;
    var opacityDim = options.state.entities.opacitySelectDim;
    var x_axisScaleType = 'linear';
    var y_axisScaleType = options.state.marker.axis_y.scaleType;
    var xAxis_which = options.state.marker.axis_x.which;
    var yAxis_which = options.state.marker.axis_y.which;
    var valueYear = options.state.time.value;

    dataset = dataset[0].filter(function (d) {
      return d['geo.cat'][0] === 'region';
    });

    d3.select('#trail-layer')
      .classed('vzb-invisible', true);

    //Returns the correct object based on string value in the state (when you change which object should be in which axis.)
    function whichSelector(value, d){
      if(value == 'lex')
        return d.lex;
      else if(value == 'gdp_per_cap')
        return d.gdp_per_cap;
      else if(value == 'pop')
        return d.pop;
      else if(value == 'time')
        return d.time;
    }

    d3.chart('LineChart', {

      initialize: function () {

        var chart = this;

        chart.x = scaleType(x_axisScaleType); //Todo: Switch from Linear to d3's Time scale.
        chart.y = scaleType(y_axisScaleType);

        chart.base.classed('Linechart', true);

        chart.layers = {};

        chart.margins = {
          top: 50,
          right: 20,
          bottom: 50,
          left: 50
        };

        chart.layers.xlabels = chart.base.append("g")
          .classed("vp-line-xaxis axis", true);

        chart.layers.ylabels = chart.base.append("g")
          .classed("vp-line-yaxis axis", true);

        chart.layers.ylabels_text = chart.base
          .append("g")
          .attr("class", "y_label_line")
          .append("text");

        chart.layers.lines = chart.base.append('g')
          .classed('vp-lines', true)
          .style("opacity", opacityRegular);

        chart.layers.scatterplots = chart.base.append('g')
          .classed('vp-scatterplot', true);

        chart.layers.dashStaticLine = chart.layers.scatterplots.append('line')
          .classed('vp-dash-line dash-line-static', true);

        chart.layers.invisibleLineX = chart.base.append("line")
          .classed('vp-dash-line vp-invi-line-x', true)
          .style("display", "none")
          .attr("y1", 100)
          .attr("y2", 100);

        chart.layers.invisibleLineY = chart.base.append("line")
          .classed('vp-dash-line vp-invi-line-y', true)
          .style("display", "none");

        chart.layers.yAxisTooltip = chart.base.append("g")
          .classed("vp-tooltip", true)
          .style("visibility", "hidden")
          .style("z-index", "10");

        chart.layers.yAxisTooltip.append('rect')
          .attr("width", 45)
          .attr("height", 100)
          .attr('fill', '#FFFFFF')
          .style('opacity', 0.90);

        chart.layers.yAxisTooltip.append("text")
          .attr('fill', '#999999')
          .attr('font-size', 14);

        chart.layers.xAxisTooltip = chart.base.append("g")
          .classed("vp-tooltip", true)
          .style("visibility", "hidden")
          .style("z-index", "11");

        chart.layers.xAxisTooltip.append('rect')
          .attr("width", 70)
          .attr("height", 20)
          .attr('fill', '#FFFFFF')
          .style('opacity', 0.93);

        chart.layers.xAxisTooltip.append("text")
          .attr('fill', '#999999')
          .attr('font-size', 14.5)
          .attr("dy", ".30em");

        chart.layers.tooltip = chart.base.append("g")
          .classed("vp-tooltip", true)
          .style("display", "none")
          .style("z-index", "11");

        /**
         * Tooltip
         */
        chart.layers.tooltip.append('rect')
          .attr("x", -(chart.margins.top * 1.5))
          .attr("y", -(chart.margins.top - 7))
          .attr("width", chart.margins.top)
          .attr("height", 36)
          .attr('fill', '#666');

        chart.layers.tooltip.append("text")
          .attr('fill', '#FFFFFF')
          .attr("x", -((chart.margins.top * 1.5) - 5))
          .attr("y", -25)
          .attr("dy", ".35em");

        function scaleType(value){
          if(value == 'log')
            return d3.scale.log();
          else if(value == 'linear')
            return d3.scale.linear();
          else if(value == 'ordinal')
            return d3.scale.ordinal();
        }

        var lineDataBind = function (data) {
          chart.xAxis = d3.svg.axis()
            .scale(chart.x).ticks(3).tickFormat(function (d, i) {
              // Todo: Dynamic number instead of 2
              if (i === 2) d = chart.maxXAxis;
              return d;
            });

          chart.layers.xlabels.call(chart.xAxis);

          chart.yAxis = d3.svg.axis()
            .scale(chart.y)
            .orient("left")
            .tickFormat(function (d, i) {
              if (i === 0) return '';
              d = Math.floor(d / 1000.0) * 1000;
              return d;
            });

          chart.layers.ylabels.call(chart.yAxis);

          chart.lineGen = d3.svg.line()
            .interpolate("basis")
            .x(function (d) {
              return chart.x(whichSelector(xAxis_which, d));
            })
            .y(function (d) {
              return chart.y(whichSelector(yAxis_which, d));
            });

          return this.selectAll('g.vz-couple-line')
            .data(chart.regionGroup);
        };

        var lineInsert = function () {
          var vzCoupleLine = this.append('g')
            .classed('vz-couple-line', true);

          vzCoupleLine.append('path')
            .classed('vp-line-shadow', true)
            .attr('stroke-width', 7);

          vzCoupleLine.append('path')
            .classed('vp-simple-line', true)
            .attr('stroke-width', 5)
            .attr('transform', 'translate(0, -2)');

          return vzCoupleLine;
        };

        function vzCoupleLineEvents(d) {
          var values = d.values;
          var bisectTime = d3.bisector(function (d) {
            return whichSelector(xAxis_which, d);
          }).left;

          this.attr('d', chart.lineGen(values))
            .attr('fill', 'none')
            .on("mouseover", function (d) {
              chart.layers.tooltip.style("display", null);
              chart.layers.dashStaticLine.style('display', 'none');
              chart.layers.invisibleLineX.style('display', null);
              chart.layers.invisibleLineY.style('display', null);

              var selectedRegion = d.key;

              chart.layers.lines.selectAll("g")
                .style("opacity", opacityDim)
                .filter(function (d) {
                  return d.key == selectedRegion;
                })
                .transition().duration(300)
                .style("opacity", opacityRegular);

              chart.layers.scatterplots.selectAll("g")
                .style("opacity", opacityDim)
                .filter(function (d) {
                  return d.geo == selectedRegion;
                })
                .transition()
                .style("opacity", opacityRegular);

              chart.layers.yAxisTooltip.style('visibility', "visible");
              chart.layers.xAxisTooltip.style('visibility', "visible");
            })
            .on("mouseout", function () {
              chart.layers.lines.selectAll("g").style("opacity", opacityRegular);
              chart.layers.scatterplots.selectAll("g").style("opacity", opacityRegular);
              chart.layers.tooltip.style("display", "none");
              chart.layers.dashStaticLine.style('display', null);
              chart.layers.invisibleLineX.style('display', 'none');
              chart.layers.invisibleLineY.style('display', 'none');
              chart.layers.yAxisTooltip.style("visibility", 'hidden');
              chart.layers.xAxisTooltip.style("visibility", "hidden");

            })
            .on("mousemove", function () {
              chart.layers.dashStaticLine.style('display', 'none');

              var x0 = chart.x.invert(d3.mouse(this)[0]);
              var i = bisectTime(values, x0, 1);
              var d0 = values[i - 1];
              var d1 = values[i];
              var d0Time = x0 - d0.time;
              var d1Time = d1.time - x0;
              var d = d0Time > d1Time ? d1 : d0;

              var cx = d3.mouse(this)[0];
              var cy = d3.mouse(this)[1];

              chart.layers.invisibleLineX.attr("y1", cy)
                .attr("x2", cx)
                .attr("y2", cy);

              chart.layers.invisibleLineY.attr("x1", cx)
                .attr("x2", cx)
                .attr("y1", cy);

              var translate = "translate(" + chart.x(whichSelector(xAxis_which, d)) +
                "," + chart.y(whichSelector(yAxis_which, d)) + ")";
              chart.layers.tooltip.attr("transform", translate);
              chart.layers.tooltip.select("text").text(Math.ceil(whichSelector(yAxis_which, d)));

              chart.layers.yAxisTooltip.select("rect").attr("x", chart.margins.left - 53).attr("y", cy - 50);
              chart.layers.yAxisTooltip.select("text").attr("x", chart.margins.left - 46).attr("y", cy).text(Math.ceil(whichSelector(yAxis_which, d)));
              chart.layers.yAxisTooltip.style('visibility', "visible");

              chart.layers.xAxisTooltip.select("rect").attr("x", cx - 35).attr("y", chart.HEIGHT - chart.margins.bottom + 7);
              chart.layers.xAxisTooltip.select("text").attr("x", cx - 19).attr("y", chart.HEIGHT - chart.margins.bottom + 17).text(whichSelector(xAxis_which, d));
            });
        }

        var _lineEnterUpdate = function () {
          var chart = this;
          this.each(function (d, i) {
            var path = d3.select(chart[0][i]).select('.vp-line-shadow');
            var path2 = d3.select(chart[0][i]).select('.vp-simple-line');

            path.attr('stroke', function () {
              return options.state.marker.color_shadow.palette[d.key];
            });

            vzCoupleLineEvents.call(path, d);

            path2.attr('stroke', function () {
              return options.state.marker.color.palette[d.key];
            });

            vzCoupleLineEvents.call(path2, d);
          });
        };

        var lineEvents = {
          exit: function () {
            this.remove();
          },
          update: _lineEnterUpdate,
          enter: _lineEnterUpdate
        };

        chart.layer('lines', chart.layers.lines, {
          dataBind: lineDataBind,
          insert: lineInsert,
          events: lineEvents
        });

        var _scatterEnterUpdate = function () {
          var fill = function (d) {
            return options.state.marker.color.palette[d.geo];
          };
          var x = function (d) {
            var xV = chart.x(whichSelector(xAxis_which, d));

            chart.layers.dashStaticLine.attr("x1", xV)
              .attr("x2", xV);

            return xV;
          };

          var textStyle = function (d) {
            this.style("fill", fill)
              .attr('x', chart.x(whichSelector(xAxis_which, d)) + 10)
              .attr('y', chart.y(whichSelector(yAxis_which, d)));
          };

          this.each(function (d, i) {
            var circle = d3.select(this[0][i]).select('circle');
            var groupLabel = d3.select(this[0][i]).select('.vp-label');
            var textName = groupLabel.select('.vp-label-name');
            var textValue = groupLabel.select('.vp-label-value');

            circle.attr('stroke-width', 2)
              .attr("r", 6)
              .attr('stroke', '#000000')
              .style("fill", fill)
              .attr("cx", x)
              .attr("cy", chart.y(whichSelector(yAxis_which, d)));

            if (d.geo == 'ame') {
              groupLabel.attr('transform', 'translate(0, 15)');
            }

            textName.text(d['geo.name']);
            textValue.text(Math.round(whichSelector(yAxis_which, d)));

            textStyle.call(textName, d);
            textStyle.call(textValue, d);
          }.bind(this));
        };

        chart.layer('scatterplots', chart.layers.scatterplots, {
          dataBind: function (data) {
            data = data.filter(function (d) {
              return whichSelector(xAxis_which, d) == chart.maxStaticXAxis;
            });

            return this.selectAll("g.vz-lc-entity")
              .data(data);
          },
          insert: function () {
            chart.layers.dashStaticLine.attr("y1", chart.margins.bottom)
              .attr("y2", (chart.HEIGHT - chart.margins.top));

            chart.layers.invisibleLineX.attr("x1", chart.margins.left)
              .attr("x2", (chart.WIDTH - chart.margins.right));

            chart.layers.invisibleLineY.attr("x1", (chart.WIDTH - chart.margins.right))
              .attr("y1", chart.margins.bottom)
              .attr("x2", (chart.WIDTH - chart.margins.right))
              .attr("y2", (chart.HEIGHT - chart.margins.top));

            var vzEntity = this.append('g')
              .classed('vz-lc-entity', true);

            vzEntity.append('circle');

            var multiText = vzEntity.append('g')
              .classed('vp-label', true);

            multiText.append('text')
              .classed('vp-label-name', true)
              .attr("x", -((chart.margins.top * 1.5) - 5))
              .attr("y", -25)
              .attr("dy", ".35em");

            multiText.append('text')
              .classed('vp-label-value', true)
              .attr("x", -((chart.margins.top * 1.5) - 5))
              .attr("y", -25)
              .attr("dy", "1.35em");

            return vzEntity;
          },
          events: {
            exit: function () {
              this.remove();
            },
            enter: _scatterEnterUpdate,
            update: _scatterEnterUpdate
          }
        });
      },

      transform: function (data) {
        // adjust our scale
        // create axis here as it depends on given data
        chart.maxXAxis = d3.max(dataset, function (d) {
          return whichSelector(xAxis_which, d);
        });
        chart.maxStaticXAxis = d3.max(data, function (d) {
          return whichSelector(xAxis_which, d);
        });
        chart.minXAxis = d3.min(dataset, function (d) {
          return whichSelector(xAxis_which, d);
        });

        var xRange = [chart.margins.left, chart.WIDTH - (chart.margins.right * 4)];

        chart.x.range(xRange).domain([chart.minXAxis, chart.maxXAxis]);

        chart.layers.xlabels.attr(
          'transform',
          'translate(0,' + (chart.HEIGHT - chart.margins.bottom) + ')'
        );

        chart.layers.ylabels_text
          .attr("x", chart.margins.left - 23)
          .attr("y", chart.margins.top - 10)
          .text("GDP per capita");

        chart.minYAxis = d3.min(dataset, function (d) {
          return whichSelector(yAxis_which, d);
        });
        chart.maxYAxis = d3.max(dataset, function (d) {
          return whichSelector(yAxis_which, d);
        });

        var yRange = [chart.HEIGHT - chart.margins.top, chart.margins.bottom];
        chart.y.range(yRange).domain([chart.minYAxis, chart.maxYAxis]).base(2).nice();

        chart.layers.ylabels.attr(
          'transform',
          'translate(' + chart.margins.left + ',0)'
        );

        // Grouping data
        chart.regionGroup = d3.nest()
          .key(function (d) {
            return d.geo;
          })
          .entries(data);

        chart.staticData = [];
        chart.regionGroup.forEach(function (d, i) {
          var keys = Object.keys(d);
          var data = {};
          data.key = d.key;
          data[yAxis_which] = d3.max(d.values, function (_d) {
            return whichSelector(yAxis_which, _d);
          });
          chart.staticData.push(data);
        });

        return data;
      },

      // properties
      width: function (newWidth) {
        if (arguments.length === 0) {
          return this.WIDTH;
        }
        this.WIDTH = newWidth;
        this.base.attr('width', newWidth);
        return this;
      },

      height: function (newHeight) {
        if (arguments.length === 0) {
          return this.HEIGHT;
        }
        this.HEIGHT = newHeight;
        this.base.attr('height', newHeight);
        return this;
      }

    });

    var chart = this.currentChart = d3.select(htmlID)
      .append("svg")
      .chart("LineChart")
      .width(this.screenSize.width)
      .height(this.screenSize.height);

    var initialData = filterArrayValueBy(dataset, 'time', valueYear);
    chart.draw(initialData);

    function filterArrayValueBy(data, key, value) {
      return data.filter(function (d) {
        return d.time <= value;
      });
    }

    var lastTime = null;

    this.Slider({
      data: dataset,
      transform: function (d, data) {
        var dsc;
        d = Math.round(d);

        if (lastTime != d) {
          dsc = filterArrayValueBy(data, 'time', d);

          if (dsc.length) {
            this.draw(dsc);
          }

          lastTime = d;
        }
      }
    });
  };
})();
